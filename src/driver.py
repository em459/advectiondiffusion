from firedrake import *
from timestepper import *
from advectiondiffusion import *

# Number of cells in each direction
n = 32

# Create a mesh
mesh = PeriodicUnitSquareMesh(n,n)

# Set up problem
# advection vector
b = 0.5,0.2
Davg = 0.02   # mean value of the diffusion coefficient
dD = 0.9*Davg # range of diffusion coefficient
nD = 4        # Number of cells of diffusion grid

problem = AdvectionDiffusionProblem(mesh,b,Davg,dD,nD)

# Save diffusion field to disk
outfile = File('output/diffusion_coefficient.pvd')
outfile.write(problem._D)

# Create initial solution (Gaussian of width sigma, centred at (x0,y0))
V_coord = FunctionSpace(mesh,"CG",1)
u0 = Function(V_coord)
x, y = SpatialCoordinate(mesh)
sigma = 0.25
x0 = 0.5
y0 = 0.5
u0.interpolate(exp(-((x-0.5)**2+(y-0.5)**2)/sigma**2))

# Time step size
dt = (1./n)**2
# Final time
tfinal = 1.0

# Monitor which writes the concentration to disk at every timestep
monitor = SaveToDiskMonitor('output/concentration.pvd')

# Integrate problem
ts = TimeStepperHeun(problem,dt,monitor)
with monitor:
    ts.integrate(u0,tfinal)
