import random
from firedrake import *
from timestepper import System

class AdvectionDiffusionProblem(System):
    '''Class representing an advection-diffusion system

    :arg mesh: underlying mesh
    :arg b: advection vector, e.g. (0.2, 0.9)
    :arg Davg: mean value of the diffusion coefficient
    :arg dD: range of diffusion coefficient, which is chosen from
             [Davg-dD,Davg+dD] in each cell
    :arg nD: Number of cells of diffusion grid
    '''
    def __init__(self,mesh,b,Davg,dD,nD):
        self._mesh = mesh
        self._V = FunctionSpace(self._mesh,"CG",1)
        self._vtest = TestFunction(self._V)
        self._b = as_vector(b)
        self._Davg = Davg
        self._dD = dD
        self._nD = nD
        self._createD()

    def _createD(self):
        '''Create a piecewise constant diffusion coefficient.

        For this the domain is split into nD x nD cells, and in each cell
        the value of D is a uniform random number in the range
        [D_avg-dD, D_avg+dD]
        '''
        random.seed(2614817)
        # The diffusion field is represented in a DG space
        V_DG = FunctionSpace(self._mesh,"DG",0)
        self._D = Function(V_DG)
        x, y = SpatialCoordinate(self._mesh)
        dDx = 1./self._nD
        # Loop over all nD x nD cells, and set a random value in each cell
        for i in range(self._nD):
            for j in range(self._nD):
                cond = conditional(And(And(And(i*dDx<=x,x<(i+1)*dDx),j*dDx<=y),y<(j+1)*dDx),1.0,0.0)
                tmpD = Function(V_DG).project(cond)
                self._D += random.uniform(self._Davg-self._dD,
                                          self._Davg+self._dD)*tmpD
        
    def V(self):
        '''Return state function space'''
        return self._V

    def resN(self, q):
        r'''Apply the non-linear part :math:`N` of the problem to a particular
        state and return :math:`N(q)` as a 1-form.

        :arg q: state to apply to
        '''
        return (inner(self._b,grad(self._vtest))*q - inner(grad(self._vtest),self._D*grad(q)))*dx

    def resM(self, q):
        r'''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.

        :arg q: state to apply to
        '''
        return q*self._vtest*dx

    def mass_solver(self, r, q):
        r'''Solver for the linear system :math:`Mq=r` for a given
        right hand side :math:`r` and the solution :math:`q`.

        :arg r: right hand side :math:`r`
        :arg q: solution
        '''
        solver_param = {'ksp_type':'cg',
                        'pc_type':'jacobi'}
        u_trial = TrialFunction(self._V)
        a_mass = self.resM(u_trial)
        lvp = LinearVariationalProblem(a_mass, r, q)
        lvs = LinearVariationalSolver(lvp,
                                      solver_parameters=solver_param)
        return lvs
