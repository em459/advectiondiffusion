from firedrake import *
import ufl
import numpy as np

from abc import ABCMeta, abstractmethod

class System(metaclass=ABCMeta):
    r'''Abstract base class describing a system
    equation of motion :math:`M q_t = N(q)` where :math:`N` is a
    non-linear operator and :math:`M` is a finite element mass matrix.

    The class provides methods for carrying out the following operations:
      * create 1-form for applying the mass matrix to a state
        i.e. return the form representing :math:`Mq`
      * create 1-form for applying the non-linear operator to a state
        i.e. return the form representing :math:`N(q)`
      * create a mass solver for the equation :math:`Mq = r`
    '''
    def __init__(self):
        pass

    @abstractmethod
    def V(self):
        '''Return state function space'''
        pass

    @abstractmethod
    def resN(self, q):
        r'''Apply the non-linear part :math:`N` of the problem to a particular
        state and return :math:`N(q)` as a 1-form.

        :arg q: state to apply to
        '''
        pass

    @abstractmethod
    def resM(self, q):
        r'''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.

        :arg q: state to apply to
        '''
        pass

    @abstractmethod
    def mass_solver(self, r, q):
        r'''Solver for the linear system :math:`Mq=r` for a given
        right hand side :math:`r` and the solution :math:`q`.

        :arg r: right hand side :math:`r`
        :arg q: solution
        '''
        pass


class TimeStepperMonitor(metaclass=ABCMeta):
    '''Abstract base class for monitor that can be used
    in the timestepper class'''
    def __init__(self):
        pass

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    @abstractmethod
    def __call__(self, t, q):
        pass

class TimeStepperMonitorDummy(TimeStepperMonitor):
    def __init__(self):
        pass
    
    def __call__(self, t, q):
        pass
    
class SaveToDiskMonitor(TimeStepperMonitor):
    r'''Timestepper monitor which saves the fields to disk at every
    timestep

    :arg filename: Name of file to write
    '''
    def __init__(self, filename, field_name=''):
        self._filename = filename
        self._field_name = field_name

    def __enter__(self):
        self._outfile = File(self._filename)

    def __call__(self, t, q):
        self._outfile.write(q, time=t)


class TimeStepper(metaclass=ABCMeta):
    r'''Solve the system

     .. math ::
        \\frac{\\partial q}{\\partial t} = N(q) + Lq

    using a particular discrete timestepping method.

    :arg system: instance of a system class, describing :math:`N`
                 and :math:`L`.
    :arg dt: time step size
    :arg monitor: monitor object which is called in each timestep
    '''
    def __init__(self, system, dt, monitor=None):
        '''
        :arg dt: time step size
        '''
        self._system = system
        self._dt = dt
        self._monitor = monitor

    @abstractmethod
    def integrate(self, q0, tfinal):
        r'''Step the initial solution :math:`q_0` to final time :math:`T`
        time steps.

        :arg q0: initial solution :math:`q_0`
        :arg tfinal: final time :math:`T`
        '''
        pass


class TimeStepperRK(TimeStepper):
    r'''
    Timestepper based on fully explicit RK methods.

    :arg system: instance of a system class, describing :math:`N`
                 and :math:`L`.
    :arg dt: time step size
    :arg monitor: monitor object which is called in each timestep
    '''
    
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperRK, self).__init__(system, dt, monitor)
        self._a = None
        self._b = None
        self.label = "RK"

    def integrate(self, q0, tfinal):
        r'''
        Implements the iteration to update the state
        :math:`q^{(n)} \\mapsto q^{(n+1)}` Note that it requires exactly
        s mass solves and s evaluations of the non-linear function.
        '''
        
        if (self._a is None):
            raise Exception("Timestepping matrices a not defined. Use derived class.")
        t = 0
        qn = Function(self._system.V())
        qn1 = Function(self._system.V())
        Mq = self._system.resM(qn)
        Q = [qn]
        NLQ = []
        Qhat = []
        solvers = []
        for i in range(self._stages-1):
            Q.append(Function(self._system.V()))
            Qhat.append(self._system.resM(qn))
        qhatn1 = self._system.resM(qn)
        for i in range(self._stages):
            NLQtmp = self._system.resN(Q[i])
            qhatn1 += self._dt*self._b[i]*NLQtmp
            NLQ.append(NLQtmp)

        for i in range(self._stages-1):
            for j in range(i+1):
                Qhat[i] += self._dt*self._a[i+1][j]*NLQ[j]
        for i in range(self._stages-1):
            solvers.append(self._system.mass_solver(Qhat[i], Q[i+1]))
        solvers.append(self._system.mass_solver(qhatn1, qn1))

        qn.assign(q0)

        if not (self._monitor is None):
            self._monitor(t, qn)
        while t < tfinal:
            for i, solver in enumerate(solvers):
                solver.solve()
            qn.assign(qn1)
            t += self._dt
            if not (self._monitor is None):
                self._monitor(t, qn)
        return qn

class TimeStepperHeun(TimeStepperRK):
    '''Two stage Heun method

    '''
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperHeun, self).__init__(system, dt, monitor)
        self._stages = 2
        self._a = as_tensor([[0, 0],
                             [1, 0]])
        self._b = as_tensor([0.5, 0.5])
        self.label = "Heun"

