\documentclass[11pt]{article}
\usepackage[margin=2cm]{geometry}
\usepackage{amssymb,amsmath}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\title{Finite element discretisation of the advection-diffusion equation}
\date{\today}
\begin{document}
\maketitle
\section{Continuum equation}
We want to solve the following equation for $u=u(\vec{x},t)$ in the domain $\Omega$:
\begin{equation}
  \partial_t u + \vec{b}\cdot \nabla u = \nabla\cdot(D\nabla u) + f.
  \label{eqn:advvec_cont}
\end{equation}
Here $\vec{b}$ is the (constant) advection vector and $D=D(\vec{x})$ is the diffusion coefficient. $f$ is the source term.
\section{Finite element discretisation}
We seek solutions $u$ to Eq. \eqref{eqn:advvec_cont} in some finite element space $V\subset H^1(\Omega)$. Multiply Eq. \eqref{eqn:advvec_cont} by a test-function $v\in V$ and integrate over the entire domain to get:
\begin{equation}
  \int_\Omega v\partial_t u\; dx + \int_\Omega v\vec{b}\cdot\nabla u\;dx = \int_\Omega v \nabla\cdot(D\nabla u)\; dx + \int_\Omega vf\;dx.
\end{equation}
Next, integrate the advection- and diffusion- terms by part to get:
\begin{equation}
  \int_\Omega v\partial_t u\; dx =
  \underbrace{\int_\Omega\nabla v\cdot \left(\vec{b}u -D\nabla u\right)\; dx}_{\text{volume integral}} + \underbrace{\int_\Omega vf \;dx}_{\text{source term}} + \underbrace{\int_{\partial \Omega} v\vec{n}\cdot\left(D\nabla u - \vec{b}u\right)\;ds}_{\text{boundary integral}}
  \label{eqn:weak_form}
\end{equation}
\paragraph{Mass conservation.} For the mass to be conserved in the absence of a source term ($f=0$), the boundary integral has to vanish. This can be seen by setting $v=1$. In this case the left hand side of Eq. \eqref{eqn:weak_form} is the rate of change of the total mass $\int_\Omega u\;dx$, and the volume integral is zero since $\nabla v=0$.

Hence, the boundary condition which guarantees mass conservation is
\begin{equation}
  \vec{n}\cdot D\nabla u=\vec{n}\cdot\vec{b}u
\end{equation}
In other words, the normal components of the advective and diffusive flux cancel.
\subsection{Weak form}
The weak form of the finite element problem in Eq. \eqref{eqn:weak_form} can be expressed in terms of two bilinear forms $m(\cdot,\cdot)$, $a(\cdot,\cdot)$ and a one-form $r(\cdot)$. More specifically, we seek $u\in V$ such that the following equation holds
\begin{equation}
  \partial_t m(v,u) = a(v,u) + F(v) \equiv N(v,u) \qquad\text{for all $v\in V$},
\end{equation}
where
\begin{equation}
  \begin{aligned}
    m(v,u) &\equiv \int_\Omega uv\;dx,\\
    a(v,u) &\equiv \int_\Omega\nabla v\cdot \left(\vec{b}u -D\nabla u\right)\; dx,\\
    F(v) &\equiv \int_\Omega vf\;dx.
  \end{aligned}
\end{equation}
\section{Code}
The resulting system $\partial m(v,u) = N(v,u)$ can be solved with any timestepping method, for example explicit Euler or the Heun method. It turns out that for this, we need to code up three things, which happens in the class \texttt{AdvectionDiffusion} in \texttt{advectiondiffusion.py}:
\begin{enumerate}
\item Evaluation of the form $N(v,u)$ for a given $u$; this is done by the class method \texttt{resN()}
\item Evaluation of the form $m(v,u)$ for a given $u$; this is done by the class method \texttt{resM()}
\item Solution of the system $m(v,u)=r(v)$ for some right hand side $r$; this is done by the class method \texttt{mass\_solver()}, which returns a Firedrake solver object.
\end{enumerate}
Note that \texttt{AdvectionDiffusion} is derived from the abstract base class \texttt{System} (defined in \texttt{timestepper.py}. The timesteppers in \texttt{timestepper.py} are able to integrate any problems which conform to the interface defined by \texttt{System}.
\end{document}
